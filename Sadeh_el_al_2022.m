function [t_peak,F_peak,F] = Sadeh_el_al_2022(gb_0,s_ft,s_KN,M0,n,epsie,epsib,p,nu,D)
% Sadeh_et_al_2022 Calculates the non-thermal emission according to Sadeh et al. 2022

%% Constants
c = 2.99792458e10;
me = 9.10938356e-28;
mp = 1.67262192369e-24;
qe = 4.80320425e-10;
re = qe^2/(me*c^2);
sigT = 8*pi/3*re^2;
Msun = 1.98847*10^33;
pc = 3.08567758e18;
kpc = 1e3*pc;
Mpc = 1e6*pc;
hr = 60*60;
day = 24*hr;
muJy = 1e-29;

%% Parameters
D = D.*Mpc;
nu = 1e9.*nu;
M0 = M0.*Msun;
MR = M0.*(gb_0).^(s_ft);
beta_0 = gb_0./sqrt(1+gb_0.^2);
gamma_0 = sqrt(1+gb_0.^2);
geratio = 1e5;
E0 = MR.*(c.^2).*E_power_law(s_ft,gamma_0,6);%fast tail kinetic energy
E = E0+M0.*(c^2).*((gb_0).^(s_KN)).*E_power_law(s_KN,sqrt(1+0.1^2),gamma_0);%Total kinetic energy

%% Dynamics
t_peak = (((1.5-sqrt(2.*((beta_0).^2)+0.25))./(beta_0.*c)).*(((3.*((MR)).*((sqrt(gamma_0.^2-1)).^(-s_ft))))./(gamma_0.*(4*pi.*n*mp))).^(1/3))./day;%Peak time in days
RR = (MR./((16*pi)*mp.*n)).^(1/3);%Normalization of the radius

%% Flux Rise
if p == 2
    lp = 1/log(geratio);
else
    lp = (p-2)./(1-geratio.^(2-p));
end
L0 = (2*pi/3).*epsie.*n.*mp*(c.^3).*(RR.^2).*lp;
gmin = (lp./(p-1)).*epsie.*mp./me;%minimal electron Lorentz factor with no time
gc = me./(4.*(4/3).*epsib.*sigT.*n.*mp.*RR);%cooling electron Lorentz factor with no time
nus = qe/me.*sqrt(2*pi.*epsib.*n*mp);%typical frequency
nuc = nus.*gc.^2;%cooling frequency with no time
num = nus.*gmin.^2;%minimum frequency with no time
q_ft = (3.*s_ft+4.5-7.5.*p)./(s_ft+5.5);%power-law rise
T_der = 1+0.7;%time derivative prefactor
Lnorm = L0.*(num.^((p-2)./2)).*(nuc.^(-0.5)).*((nu).^((1-p)./2));
F_rise = @(t)(1./(4*pi.*D.^2)).*T_der.*((1.7).^(-2.5*p+1)).*(7.7).*(1.25-0.35*p).*Lnorm.*((t./(RR./c)).^(q_ft));%Flux rise
F_peak = (1/muJy).*F_rise(t_peak.*day);%Peak flux in \muJy

%% ST
zeta = 1.15;%sedov constant for gamma = 5/3
t_ST = (0.05*((2.5*c)/(zeta)).*((E./(mp.*n)).^(-0.2))).^(-5/3);%ST time
L0_ST = lp.*2*pi.*epsie.*(zeta^5).*(0.4^3).*((10/3)/(64/9)).*E./t_ST;
bmin_ST = (9/32)*(0.05^2).*(lp./(p-1)).*epsie.*mp./me;
num_ST = (3*sqrt(pi)*qe*0.05*sqrt(n.*mp.*epsib))./(4*me);
nuc_ST = num_ST.*(((8*me)./(9*(4/3)*sigT.*epsib.*n.*mp.*c.*(0.05^2).*t_ST)).^2);
F_ST = L0_ST.*(bmin_ST.^(p-2)).*(num_ST.^(p./2-1)).*(nuc_ST.^(-1/2)).*(nu.^((1-p)./2)).*(1./(4*pi.*D.^2));%ST flux
q_ST = (21-15.*p)./10;

%% Full light curve
ip = 5;
q_KN = (3.*s_KN+7.5-7.5.*p)./(s_KN+4.7);
F = @(t)(1/muJy)*((0.5.*(F_rise(t_peak.*day)).^(-ip)).*((t./(t_peak)).^(-ip.*q_ft)+(t./(t_peak)).^(-ip.*q_KN))+(F_ST.*((t./(t_ST./day)).^q_ST)).^(-ip)).^(-1./ip);%Interpolation

%% inner function
    function E_ejecta = E_power_law(s,gamma_min,gamma_max)
        %normalized ejecta kinetic energy given a power law mass profile, without mass
        for i = 1:length(s)
            Func = @(gam)s(i).*((gam)./(gam+1)).*((sqrt(gam.^2-1)).^(-s(i)));%Integrand of the ejecta energy
            if length(gamma_min)>1
                E_ejecta(i) = arrayfun(@(gam)integral(Func,gam,gamma_max),gamma_min);%Ejecta energy
            elseif length(gamma_max)>1
                E_ejecta(i) = arrayfun(@(gam)integral(Func,gamma_min,gam),gamma_max);%Ejecta energy
            else
                E_ejecta(i) = integral(Func,gamma_min,gamma_max);%Ejecta energy
            end
        end
    end

end

